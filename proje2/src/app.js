const demoState = [{
    _id: uuidv1(),
    firstName: "Ateş",
    lastName: "Etiksan",
    classroom: "arılar"
},{
    _id: uuidv1(),
    firstName: "Hasan",
    lastName: "Demir",
    classroom: "kelebekler"
},{
    _id: uuidv1(),
    firstName: "Kerem",
    lastName: "Tekin",
    classroom: "arılar"
}]

// class StudentRow extends React.Component {

//     render () {
//         return (
//             <tr>
//                 <td>{this.props.student.firstName}</td>
//                 <td>{this.props.student.lastName}</td>
//                 <td>{this.props.student.classroom}</td>
//                 <td>
//                     <button onClick={(e) => {
//                         this.props.onRemove(this.props.student)
//                     }}>SİL</button>
//                 </td>
//             </tr>
//         )
//     }
// }

// Functional Component
// Stateless Component
const StudentRow = (props) => {

    const {student,onRemove} = props
    
    return (
        <tr>
            <td>{student.firstName}</td>
            <td>{student.lastName}</td>
            <td>{student.classroom}</td>
            <td>
                <button onClick={() => {
                    onRemove(student)
                }}>SİL</button>
            </td>
        </tr>
    )
}

class StudentList extends React.Component {

    constructor () {
        super()

        this.state = {
            students: demoState
        }

        this.onRemove = this.onRemove.bind(this)
    }

    // Lifecycle metotları
    componentDidMount () {
        console.log('student list yüklendi')
    }

    onRemove (student) {
        this.setState((prevState) => {

            const students = prevState.students.filter((item) => {
                return item._id != student._id
            })

            return {
                students
            }
        })
    }

    render () {

        const {students} = this.state

        return (
            <>
                <h1>Student List</h1>
                <table>
                    <tbody>
                    {
                        students.map((item) => {
                            return (
                                <StudentRow 
                                    key={item._id} 
                                    student={item} 
                                    onRemove={this.onRemove} 
                                />
                            )
                        })
                    }
                    </tbody>
                </table>
            </>
        )
    }
}

const root = document.getElementById('app')
ReactDOM.render(<StudentList />, root)