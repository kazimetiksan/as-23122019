"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var demoState = [{
  _id: uuidv1(),
  firstName: "Ateş",
  lastName: "Etiksan",
  classroom: "arılar"
}, {
  _id: uuidv1(),
  firstName: "Hasan",
  lastName: "Demir",
  classroom: "kelebekler"
}, {
  _id: uuidv1(),
  firstName: "Kerem",
  lastName: "Tekin",
  classroom: "arılar"
}]; // class StudentRow extends React.Component {
//     render () {
//         return (
//             <tr>
//                 <td>{this.props.student.firstName}</td>
//                 <td>{this.props.student.lastName}</td>
//                 <td>{this.props.student.classroom}</td>
//                 <td>
//                     <button onClick={(e) => {
//                         this.props.onRemove(this.props.student)
//                     }}>SİL</button>
//                 </td>
//             </tr>
//         )
//     }
// }
// Functional Component
// Stateless Component

var StudentRow = function StudentRow(props) {
  var student = props.student,
      onRemove = props.onRemove;
  return React.createElement("tr", null, React.createElement("td", null, student.firstName), React.createElement("td", null, student.lastName), React.createElement("td", null, student.classroom), React.createElement("td", null, React.createElement("button", {
    onClick: function onClick() {
      onRemove(student);
    }
  }, "S\u0130L")));
};

var StudentList =
/*#__PURE__*/
function (_React$Component) {
  _inherits(StudentList, _React$Component);

  function StudentList() {
    var _this;

    _classCallCheck(this, StudentList);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(StudentList).call(this));
    _this.state = {
      students: demoState
    };
    _this.onRemove = _this.onRemove.bind(_assertThisInitialized(_this));
    return _this;
  } // Lifecycle metotları


  _createClass(StudentList, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      console.log('student list yüklendi');
    }
  }, {
    key: "onRemove",
    value: function onRemove(student) {
      this.setState(function (prevState) {
        var students = prevState.students.filter(function (item) {
          return item._id != student._id;
        });
        return {
          students: students
        };
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var students = this.state.students;
      return React.createElement(React.Fragment, null, React.createElement("h1", null, "Student List"), React.createElement("table", null, React.createElement("tbody", null, students.map(function (item) {
        return React.createElement(StudentRow, {
          key: item._id,
          student: item,
          onRemove: _this2.onRemove
        });
      }))));
    }
  }]);

  return StudentList;
}(React.Component);

var root = document.getElementById('app');
ReactDOM.render(React.createElement(StudentList, null), root);
