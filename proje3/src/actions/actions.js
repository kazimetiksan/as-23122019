import uuid from 'uuid'

export const setAllAction = (data) => {
    return {
        type: 'SET_ALL',
        data
    }
}

export const addAction = ({
    firstName='defaultFirst',
    lastName='defaultLast',
    classroom='defaultClass'
}) => {
    return {
        type: 'ADD',
        data: {
            _id: uuid(),
            firstName,
            lastName,
            classroom
        }
    }
}

export const editAction = (editingId,{
    firstName='defaultFirst',
    lastName='defaultLast',
    classroom='defaultClass'
}) => {
    return {
        type: 'EDIT',
        editingId,
        data: {firstName,lastName,classroom}
    }
}

export const removeAction = (removingId) => {
    return {
        type: 'REMOVE',
        removingId
    }
}