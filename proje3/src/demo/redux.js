console.log('redux js loaded')

import {createStore} from 'redux'

const demoState = {
    count: 1
}

// Redux Reducer
const reducer = (state=demoState,action) => {
    
    if (action.type == 'INCREMENT') {
        return {
            count: state.count+action.incrementBy
        }
    }
    
    else if (action.type == 'DECREMENT') {
        return {
            count: state.count-action.decrementBy
        }
    }

    return state
}

const store = createStore(reducer)

console.log(store.getState())

// Redux Action
const incrementAction = (incrementBy=1) => {
    return {
        type: 'INCREMENT',
        incrementBy
    }
}

store.dispatch(incrementAction(20))

console.log(store.getState())

store.dispatch({
    type: 'DECREMENT',
    decrementBy: 5
})

console.log(store.getState())