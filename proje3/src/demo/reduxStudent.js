import uuid from 'uuid'
import {createStore} from 'redux'

const demoState = [{
    _id: uuid(),
    firstName: "Ateş",
    lastName: "Etiksan",
    classroom: "arılar"
},{
    _id: uuid(),
    firstName: "Hasan",
    lastName: "Demir",
    classroom: "kelebekler"
},{
    _id: uuid(),
    firstName: "Kerem",
    lastName: "Tekin",
    classroom: "arılar"
}]

const studentReducer = (state=demoState,action) => {

    switch (action.type) {

        case 'ADD':
            return [...state,action.data]

        case 'EDIT':
            return state.map((item) => {

                if (item._id == action.editingId) {
                    return {
                        ...item,
                        ...action.data
                    }
                }

                return item
            })

        case 'REMOVE':
            return state.filter((item) => {
                return action.removingId != item._id
            })

        default: 
            return state
    }
}

const store = createStore(studentReducer)

console.log('start',store.getState())

const addAction = ({
    firstName='defaultFirst',
    lastName='defaultLast',
    classroom='defaultClass'
}) => {
    return {
        type: 'ADD',
        data: {
            _id: uuid(),
            firstName,
            lastName,
            classroom
        }
    }
}

store.dispatch(addAction({
    firstName: "Elif",
    lastName: "Demir"
}))

console.log('appended',store.getState())

store.dispatch(addAction({
    firstName: "Leyla",
    lastName: "Tekin",
    classroom: "kelebekler"
}))

console.log('appended',store.getState())

const editingId = store.getState()[0]._id

const editAction = (editingId,{
    firstName='defaultFirst',
    lastName='defaultLast',
    classroom='defaultClass'
}) => {
    return {
        type: 'EDIT',
        editingId,
        data: {firstName,lastName,classroom}
    }
}

store.dispatch((editAction(editingId,{
    firstName: "Ayşe",
    lastName: "Demir",
    classroom: "arılar"
})))

console.log('edited 0',store.getState())

const removingId = store.getState()[0]._id

const removeAction = (removingId) => {
    return {
        type: 'REMOVE',
        removingId
    }
}

store.dispatch(removeAction(removingId))

console.log('removed 0',store.getState())