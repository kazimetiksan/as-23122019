import React from 'react'
import ReactDOM from 'react-dom'

// ekranlar
import Dashboard from './components/Dashboard'
import AddNew from './components/AddNew'
import Edit from './components/Edit'
import Header from './components/Header'

// routing
import {BrowserRouter,Route,Switch} from 'react-router-dom'

// redux
import {Provider} from 'react-redux'
import {configureStore} from './store/configureStore'
import {setAllAction} from './actions/actions'

// API
import axios from 'axios'

const store = configureStore()

// Promise Yapısı
axios.get('https://std02.herokuapp.com/api/student')
.then((response) => {
    // başarılı bağlantı sonucu

    console.log(response.data)

    store.dispatch(setAllAction(response.data.list))
})
.catch((err) => {
    // bağlantıs hatası
})
.finally(() => {
    // her durumda çalışacak kodlar
})

const App = () => {

    return (
        <Provider store={store}>
            <BrowserRouter>
                <Header />
                <Switch>
                    <Route path="/" component={Dashboard} exact={true} />
                    <Route path="/add" component={AddNew} />
                    <Route path="/edit/:id" component={Edit} />
                </Switch>
            </BrowserRouter>
        </Provider>
    )
}

const root = document.getElementById('app')
ReactDOM.render(<App />, root)