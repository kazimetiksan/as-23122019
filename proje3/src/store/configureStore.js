import {studentReducer} from '../reducers/reducers'
import {createStore} from 'redux'

export const configureStore = () => {
    return createStore(studentReducer)
}