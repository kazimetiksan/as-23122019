import uuid from 'uuid'

// const demoState = [{
//     _id: uuid(),
//     firstName: "Ateş",
//     lastName: "Etiksan",
//     classroom: "arılar"
// },{
//     _id: uuid(),
//     firstName: "Ayşe",
//     lastName: "Demir",
//     classroom: "kelebekler"
// },{
//     _id: uuid(),
//     firstName: "Kerem",
//     lastName: "Tekin",
//     classroom: "arılar"
// }]

export const studentReducer = (state=[],action) => {

    switch (action.type) {

        case 'SET_ALL':
            return action.data

        case 'ADD':
            return [...state,action.data]

        case 'EDIT':
            return state.map((item) => {

                if (item._id == action.editingId) {
                    return {
                        ...item,
                        ...action.data
                    }
                }

                return item
            })

        case 'REMOVE':
            return state.filter((item) => {
                return action.removingId != item._id
            })

        default: 
            return state
    }
}