import React from 'react'
import StudentRow from './StudentRow'
import {connect} from 'react-redux'
import {removeAction} from '../actions/actions'
import {withRouter} from 'react-router-dom'

class StudentList extends React.Component {

    constructor () {
        super()

        this.onRemove = this.onRemove.bind(this)
        this.onEdit = this.onEdit.bind(this)
    }

    componentDidMount () {
        console.log('student list yüklendi',this.props)
    }

    onRemove (student) {

        const {removeStudent} = this.props
        removeStudent(student._id)
    }

    onEdit (editingStudent) {

        const {history} = this.props
        history.push(`/edit/${editingStudent._id}`)
    }

    render () {

        const {students} = this.props

        return (
            <>
                <h1>Student List</h1>
                <table>
                    <tbody>
                    {
                        students.map((item) => {
                            return (
                                <StudentRow 
                                    key={item._id} 
                                    student={item} 
                                    onEdit={this.onEdit}
                                    onRemove={this.onRemove} 
                                />
                            )
                        })
                    }
                    </tbody>
                </table>
            </>
        )
    }
}

// Higher Order Component

const mapStateToProps = (state) => {
    return {
        students: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        removeStudent: (_id) => {dispatch(removeAction(_id))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(
    withRouter(StudentList)
)
