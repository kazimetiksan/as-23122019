import React from 'react'
import {NavLink} from 'react-router-dom'

const Header = () => {

    return (
        <div>
            <NavLink to="/">Dashboard</NavLink>&nbsp;&nbsp;&nbsp;
            <NavLink to="/add">Add</NavLink>
        </div>
    )
}

export default Header