import React from 'react'
import AddForm from './AddForm'
import {connect} from 'react-redux'
import {addAction} from '../actions/actions'

const AddNew = (props) => {

    const onAdd = (newStudent,editingStudent=undefined) => {

        // const {addStudent,history} = props
        props.addStudent(newStudent)
        props.history.push('/')
    }

    return (
        <>
            <h1>Yeni Ekle</h1>
            <AddForm onAdd={onAdd} />
        </>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        addStudent: (data) => {dispatch(addAction(data))}
    }
}

export default connect(null,mapDispatchToProps)(AddNew)