import React from 'react'

const StudentRow = (props) => {

    const {student,onRemove,onEdit} = props
    
    return (
        <tr>
            <td>{student.firstName}</td>
            <td>{student.lastName}</td>
            <td>{student.classroom}</td>
            <td>
                <button onClick={() => {
                    onEdit(student)
                }}>GÜNCELLE</button>
            </td>
            <td>
                <button onClick={() => {
                    onRemove(student)
                }}>SİL</button>
            </td>
        </tr>
    )
}

export default StudentRow