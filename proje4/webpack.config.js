const path = require('path');
const TerserPlugin = require('terser-webpack-plugin'); // ES5 NodeJS

module.exports = (env='development') => {

    console.log('webpack running mode: ',env)

    return {
        mode: env,
        entry: './src/app.js',
        output: {
            path: path.resolve(__dirname, 'public'),
            filename: 'script.js'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader'
                    }
                },
                {
                    test: /\.css$/i,
                    use: ['style-loader', 'css-loader']
                }
            ]
        },
        devServer: {
            contentBase: path.resolve(__dirname, 'public'),
            historyApiFallback: true
        },
        optimization: {
            minimize: env == 'production',
            minimizer: [new TerserPlugin()],
        },
    };
}