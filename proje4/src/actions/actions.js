import axios from 'axios'

export const setAllAction = (data) => {
    return {
        type: 'SET_ALL',
        data
    }
}

export const asyncAddAction = ({
    firstName='defaultFirst',
    lastName='defaultLast', 
    classroom='defaultClass'
},callback) => {

    return (dispatch) => {

        axios.post('https://std02.herokuapp.com/api/student',{
            firstName,
            lastName,
            classroom
        })
        .then((response) => {

            const newStudent = response.data.data[0]
            dispatch(addAction(newStudent))
            callback(response.data.status.result)
        })
        .catch((err) => {
            callback(false)
            console.log(err)
        })
    }
}

export const addAction = (data) => {
        return {
            type: 'ADD',
            data
        }
}

export const asyncEditAction = (editingId,data,callback) => {

    return (dispatch) => {

        const url = `https://std02.herokuapp.com/api/student/${editingId}`

        axios.patch(url,data)
        .then((response) => {
            console.log('updated', response.data)
            dispatch(editAction(editingId,response.data.data[0]))
            callback(true)
        })
        .catch((err) => {
            console.log(err)
            callback(false)
        })
    }
}
export const editAction = (editingId,{
    firstName='defaultFirst',
    lastName='defaultLast',
    classroom='defaultClass'
}) => {
    return {
        type: 'EDIT',
        editingId,
        data: {firstName,lastName,classroom}
    }
}

export const asyncRemoveAction = ({removingId}) => {

    return (dispatch) => {

        const url = `https://std02.herokuapp.com/api/student/${removingId}`

        axios.delete(url)
        .then((response) => {
            dispatch(removeAction(removingId))
        })
        .catch((err) => {
            console.log(err)
        })    
    }
}
export const removeAction = (removingId) => {
    return {
        type: 'REMOVE',
        removingId
    }
}