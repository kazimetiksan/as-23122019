// Array Spread

const sehirler = ["istanbul","ankara"]
const sehirler2 = [...sehirler,"izmir"]

// Object Spread

const person = {
    firstName: "Kazım",
    lastName: "Etiksan",
    classroom: "abc"
}

const edited = {
    firstName: "Hasan",
    age: 30
}

const newPerson = {
    ...person,
    ...edited
}

console.log(newPerson)

// {
//     firstName: "Hasan",
//     lastName: "Etiksan",
//     classroom: "abc",
//       age: 30
// }

