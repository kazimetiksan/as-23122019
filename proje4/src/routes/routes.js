import React from 'react'
import {Route,Redirect} from 'react-router-dom'

const isLoggedIn = () => {

    const xauth = sessionStorage.getItem('xauth')
    return xauth != undefined
}

export const Private = ({component:RoutedComponent,...routedProps}) => {
    return (
        isLoggedIn() ?
            <Route render={(props) => {
                return <RoutedComponent {...props} />
            }} {...routedProps} />
        :
            <Redirect to="/signin" />
    )
}