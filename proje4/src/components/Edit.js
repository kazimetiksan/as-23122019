import React from 'react'
import AddForm from './AddForm'
import {connect} from 'react-redux'
import {asyncEditAction} from '../actions/actions'

const Edit = (props) => {

    console.log('editlenecek id', props.match.params.id)

    const editingStudent = () => {
        const {students,match} = props
        return students.find((item) => {
            return item._id == match.params.id
        })
    }

    const onAdd = (data,editingStudent) => {

        const {updateStudent,history} = props
        updateStudent(editingStudent._id,data,(success) => {
            if (success) {
                history.push('/')
            } else {
                alert('hata!!')
            }
        })
        
    }

    return (
        <>
            <h1>Güncelle</h1>
            <AddForm 
                editingStudent={editingStudent()} 
                onAdd={onAdd}
            />
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        students: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateStudent: (_id,data,callback) => {dispatch(asyncEditAction(_id,data,callback))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Edit)