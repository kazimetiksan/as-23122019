import React, {useState,useEffect} from 'react'
import validator from 'validator'
import axios from 'axios'

// Functional / Stateless Component
const SignUp = () => {

    const [email,setEmail] = useState('')
    const [name,setName] = useState('')
    const [password,setPassword] = useState('')

    // component did mount
    useEffect(() => {
        console.log('component did mount')
    }, [])

    // component did update --> email
    useEffect(() => {
        console.log('state did update email')
        console.log({
            email,
            name,
            password
        })
    }, [email])

    const onSubmit = (e) => {

        e.preventDefault()

        if (!validator.isEmail(email)) {

            alert('email hatalı!!')
            return
        }
        
        const newUser = {
            email,
            name,
            password
        }

        axios.post('https://std02.herokuapp.com/api/users',newUser)
        .then((response) => {
            console.log(response)
        })
    }

    return (
        <>
            <h1>Yeni Kayıt</h1>
            <div>
                <form onSubmit={onSubmit}>
                    <input name="email" placeholder="Email yazın.." value={email} onChange={(e) => {
                        setEmail(e.target.value)
                    }} /><br />
                    <input name="name" placeholder="İsim yazın.." value={name} onChange={(e) => {
                        setName(e.target.value)
                    }} /><br />
                    <input name="password" placeholder="Password yazın.." value={password} onChange={(e) => {
                        setPassword(e.target.value)
                    }} /><br />
                    <button>Kaydet</button>
                </form>
            </div>
        </>
    )
}

export default SignUp