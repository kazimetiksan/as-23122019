import React from 'react'

const AddForm = (props) => {

    const {editingStudent,onAdd} = props

    const onSubmit = (e) => {
        e.preventDefault()

        const firstName = e.target.elements.firstName.value
        const lastName = e.target.elements.lastName.value
        const classroom = e.target.elements.classroom.value

        const newStudent = {
            firstName,
            lastName,
            classroom
        }

        onAdd(newStudent,editingStudent)
    }

    return (
        <>
            <div>
                <form onSubmit={onSubmit}>
                    <input 
                        name="firstName" 
                        placeholder="Ad yazın.." 
                        defaultValue={editingStudent ? editingStudent.firstName : ''}
                    /><br />
                    <input 
                        name="lastName" 
                        placeholder="Soyad yazın.." 
                        defaultValue={editingStudent ? editingStudent.lastName : ''}
                    /><br />
                    <input 
                        name="classroom" 
                        placeholder="Sınıf yazın.." 
                        defaultValue={editingStudent ? editingStudent.classroom : ''}
                    /><br />
                    <button>{editingStudent == undefined ? 'Ekle' : 'Güncelle'}</button>
                </form>
            </div>
        </>
    )
}

export default AddForm