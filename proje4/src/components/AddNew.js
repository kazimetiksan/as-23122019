import React from 'react'
import AddForm from './AddForm'
import {connect} from 'react-redux'
import {addAction,asyncAddAction} from '../actions/actions'

const AddNew = (props) => {

    const onAdd = (newStudent,editingStudent=undefined) => {

        // const {addStudent,history} = props
        props.addStudent(newStudent,(success) => {
            if (success) {
                props.history.push('/')
            } else {
                alert('hata!!')
            }
        })
    }

    return (
        <>
            <h1>Yeni Ekle</h1>
            <AddForm onAdd={onAdd} />
        </>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        addStudent: (data,callback) => {dispatch(asyncAddAction(data,callback))}
    }
}

export default connect(null,mapDispatchToProps)(AddNew)