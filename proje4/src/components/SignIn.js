import React, {useState} from 'react'
import validator from 'validator'
import axios from 'axios'

// Functional / Stateless Component
const SignIn = () => {

    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')

    const onSubmit = (e) => {

        e.preventDefault()

        if (!validator.isEmail(email)) {
            alert('email hatalı!!')
            return
        }
        
        const newUser = {
            email,
            password
        }

        axios.post('https://std02.herokuapp.com/api/users/login',newUser)
        .then((response) => {
            const xauth = response.headers.xauth
            sessionStorage.setItem('xauth',xauth)

            const userInfo = response.data.data[0]
            sessionStorage.setItem('userInfo',JSON.stringify(userInfo))
        })
    }

    return (
        <>
            <h1>Giriş Yap</h1>
            <div>
                <form onSubmit={onSubmit}>
                    <input name="email" placeholder="Email yazın.." value={email} onChange={(e) => {
                        setEmail(e.target.value)
                    }} /><br />
                    <input name="password" placeholder="Password yazın.." value={password} onChange={(e) => {
                        setPassword(e.target.value)
                    }} /><br />
                    <button>Giriş Yap</button>
                </form>
            </div>
        </>
    )
}

export default SignIn