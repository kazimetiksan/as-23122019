import React from 'react'
import {NavLink} from 'react-router-dom'

const Header = () => {

    const userInfo = JSON.parse(sessionStorage.getItem('userInfo'))

    return (
        <div>
            <NavLink to="/">Dashboard</NavLink>&nbsp;&nbsp;&nbsp;
            <NavLink to="/add">Add</NavLink>&nbsp;&nbsp;&nbsp;
            <NavLink to="/signup">Kayıt Ol</NavLink>&nbsp;&nbsp;&nbsp;
            {
                userInfo && 
                <span>Kullanıcı Adı: {userInfo.name}</span>
            }
        </div>
    )
}

export default Header