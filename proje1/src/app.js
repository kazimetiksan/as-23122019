class Counter extends React.Component {

    constructor (props) {
        super(props)

        this.onIncrease = this.onIncrease.bind(this)
        this.onDecrease = this.onDecrease.bind(this)

        this.state = {
            count: props.startsWith
        }
    }

    onDecrease () {
        this.setState((prevState) => {
            return {
                count: prevState.count-1
            }
        })
    }

    onIncrease () {

        this.setState((prevState) => {
            return {
                count: prevState.count+1
            }
        }, () => {
            console.log(this.state.count)
        })
    }

    render () {

        console.log(this.props)

        return (
            <div>
                <h1>Hello World</h1>
                <h2>Current Count: {this.state.count}</h2>
                <button onClick={this.onDecrease}>Azalt</button>
                <button onClick={this.onIncrease}>Arttır</button>
            </div>
        )
    }
}

const root = document.getElementById('app')

ReactDOM.render(<Counter startsWith={10} />, root)